# An introduction for using docker images as pytorch script runtimes
This **Image/Dockerfile** aims to create a container for pytorch runtimes.

## How to use?
* You should git clone this repository first.
* And then cd into the folder, type as follows according to your cuda version and torch version.
```
sudo docker build --no-cache -f Dockerfile-cuda9.2-py37-torch1.4 -t pytorch_env/pytorch:1804-c9_2-t1_4 .
```
* For your pytorch scripts, you can use as the hereunder part.
If you ran your previous python code as 
```
python3 a.py
```
* Now you should run as :
```
sudo docker run -it --rm -v "$PWD":/home/ubuntu -w /home/ubuntu pytorch_env/pytorch:1804-c9_2-t1_4 python3 a.py
```
